def docker_registry = 'docker.io'
def selectedImageKey = ''
def repo = ''
def token = ''
def prms = ''
def dhub_repo = ''
def dhub_tags = ''

pipeline {
    agent any

        parameters {
        choice(name: 'IMAGE', choices: ['vteam', 'vsocial', 'vshared', 'vproject', 'vprofile', 'vpartner', 'vnotification', 'vinui', 'vinoauth', 'vcompany', 'vchat', 'vboard', 'landing'] , description: 'Select the image')
        string(name: 'GIT_TAG', defaultValue: '', description: 'Enter the release tag')
        booleanParam(name: 'OVERRIDE', defaultValue: false)
        }

    stages {
        stage('Selected Image') {
            steps {
                script {
                    sh "echo ${docker_registry}/virtualizedin/${params.IMAGE}:${params.GIT_TAG}"
                    selectedImageKey = params.IMAGE
                    switch (selectedImageKey) {
                        case 'vteam':
                            repo = 'team'
                            break
                        case 'vsocial':
                            repo = 'social_be'
                            break
                        case 'vshared':
                            repo = 'shared-resources'
                            break
                        case 'vproject':
                            repo = 'vproject'
                            break
                        case 'vprofile':
                            repo = 'vUser'
                            break
                        case 'vpartner':
                            repo = 'vPartner'
                            break
                        case 'vnotification':
                            repo = 'vnotification'
                            break
                        case 'vinui':
                            repo = 'vin_fe'
                            break
                        case 'vinoauth':
                            repo = 'vauth-ts'
                            break
                        case 'vcompany':
                            repo = 'vCompany'
                            break
                        case 'vchat':
                            repo = 'chat'
                            break
                        case 'vboard':
                            repo = 'board'
                            break
                        case 'landing':
                            repo = 'landing-page-typescript'
                            break
                        default:
                            error 'Invalid image selection'
                    }
                    sh "echo image is : ${selectedImageKey} and repo is : ${repo} and tag is : ${params.GIT_TAG} "
                }
            }
        }

        stage('Checkout the Git repo') {
            steps {
                checkout([$class: 'GitSCM',
                    branches: [[name: "${params.GIT_TAG}"]],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    gitTool: 'Default',
                    submoduleCfg: [],
                    userRemoteConfigs: [[url: "git@gitlab.com:vin_mvp/${repo}.git", credentialsId: 'vin-pkey']]
                ])
            }
        }

        stage('Check if the tag exists in the docker hub registry') {
            steps {
                script {
                    if (params.OVERRIDE == true) {
                        echo 'Skipping'
                    } else {
                        dhub_repo = "virtualizedin/${selectedImageKey}"
                        prms = "service=registry.docker.io&scope=repository:${dhub_repo}:pull"

                        withCredentials([usernamePassword(credentialsId: 'docker_hub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                            token = sh(script:"curl --user ${USERNAME}:${PASSWORD} 'https://auth.docker.io/token?${prms}' | jq -r '.token'",  returnStdout: true).trim()
                        }

                        dhub_tags = sh(script:"curl 'https://registry-1.docker.io/v2/${dhub_repo}/tags/list' -H 'Authorization: Bearer ${token}' | jq '.tags'", returnStdout: true)

                        if (dhub_tags.contains("${GIT_TAG}") == false) {
                            echo 'The tag not exist and it will be created'
                        } else {
                            echo 'Tag exist and it will not be created'
                            currentBuild.result = 'FAILURE'
                            return
                        }
                    }
                }
            }
        }

        stage('Build the code') {
            steps {
                sshagent(credentials: ['vframe']) {
                    sh '/usr/bin/git ls-remote -h -t ssh://git@gitlab.com/vin_mvp/vin_framework.git'
                    sh "DOCKER_BUILDKIT=1 docker build --no-cache --ssh default=$SSH_AUTH_SOCK -t ${docker_registry}/virtualizedin/${selectedImageKey}:${params.GIT_TAG} ."
                }
            }
        }

        stage('Push the code') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker_hub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    script {
                        sh 'echo $PASSWORD | docker login -u $USERNAME --password-stdin $docker_registry'
                        sh "docker push ${docker_registry}/virtualizedin/${selectedImageKey}:${params.GIT_TAG}"
                    }
                }
            }
        }

        stage('Clean up') {
            steps {
                sh "docker rmi ${docker_registry}/virtualizedin/${selectedImageKey}:${params.GIT_TAG}"
                sh "docker logout ${docker_registry}"
            }
        }
    }
}
    // if(dhub_tags.contains("${GIT_TAG}")) {
    //     sh(script:"curl 'https://registry-1.docker.io/v2/${dhub_repo}/manifests/${GIT_TAG}' -H 'Authorization: Bearer ${token}' | jq '.fsLayers'")

    // // sh (script:"curl -X PUT -H 'Authorization: JWT ${token}' --data-binary '@manifest.json' https://registry-1.docker.io/v2/${dhub_repo}/manifests/${GIT_TAG}")
    // }
    // else {
    //     echo 'not found'
    // }